\documentclass[10pt,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics
\usepackage[english]{babel} % Language hyphenation and typographical rules
\usepackage{algpseudocode}
\usepackage{algorithm}

% New definitions
\algnewcommand\algorithmicswitch{\textbf{switch}}
\algnewcommand\algorithmiccase{\textbf{case}}
\algnewcommand\algorithmicassert{\texttt{assert}}
\algnewcommand\Assert[1]{\State \algorithmicassert(#1)}%
% New "environments"
\algdef{SE}[SWITCH]{Switch}{EndSwitch}[1]{\algorithmicswitch\ #1\ \algorithmicdo}{\algorithmicend\ \algorithmicswitch}%
\algdef{SE}[CASE]{Case}{EndCase}[1]{\algorithmiccase\ #1}{\algorithmicend\ \algorithmiccase}%
\algtext*{EndSwitch}%
\algtext*{EndCase}%
\title{Percolation of square-lattices in two-dimensions using shared and distributed memory architectures}
\author{Nicholas Pritchard and Callum Sullivan}
\begin{document}
\maketitle
\part{Algorithm Description}
\chapter{Introduction and Problem Description}
In describing out approach to this problem, we believe it important to briefly establish our interpretation of the problem description for the main purpose of introducing the terminology we use.\newline
In a concise form, we populate a two-dimensional lattice (array) based on one of two paradigms: site-population where each array index is marked as occupied if a random number is less than the provided percolation probability, and bond-percolation where each edge of the array is considered as a site. To facilitate this, one major decision was to represent both matrices as edge-lists to allow for a single search implementation for both population methods.\newline
We have implemented a simple naive algorithm for determining percolation in parallel where each processor and thread start from differing locations. We also specify a more sophisticated algorithm based on the well known Hoshen-Kopelman algorithm, the basic premise of this approach is outlined below.\newline
We split the lattice into strips vertically where the first and last column is replicated in neighbouring nodes. For this reason, the master-node will populate these shared columns initially while the rest of the lattice will be populated by its respective node. After the edge-lists have been generated each node will label the clusters it finds locally using a parallel-implementation of the Hoshen-Kopelman algorithm \cite{teuler}. After this stage the nodes will enter a relaxation phase where information and request packets are prepared and passed to each nodes neighbours informing of common clusters. In precisely $log(P)$ rounds of messaging (where $P$ is the number of processor nodes) the cluster labelling will now be globally consistent.\newline
Finally an additional information gathering phase will be completed where nodes request information such as size and boundary-conditions from their neighbours finally passing this information to the master node to be processed and displayed.\newline
In either case the information we intend to gain from this process is as follows:
\begin{itemize}
\item Whether the lattice percolates horizontally (spans every column)
\item Whether the lattice percolates vertically (spans every row)
\item If both a true, the lattice percolates entirely
\item In every case, report the size of the largest cluster found in terms of lattice-sites
\item Additionally, we report any performance metrics (such as time taken) when requested at run-time. 
\end{itemize} 
We have considered the wrap-around property of the lattice as additional potential edges between the boundaries of the array and are considered the same as any other element in the lattice.
\chapter{The Na\"ive Algorithm}
Our na\"ive algorithm is heavily based off of our parallel approach in part-I. To quickly summarise, we populate the lattice as edge-lists either by considering site or edges. We then perform a depth-first-search keeping track of the largest cluster seen thus far and whether it percolates horizontally and/or vertically and thus totally. The depth-first search is parallelised by allowing threads to begin from differing locations. We thus save time when threads discover disjoint-clusters and at worst perform serially if multiple threads encounter the same cluster from different locations; the cluster is searched multiple times.\newline
However, in moving to MPI we have extended this basic algorithm to allow for multiple processors.\newline
Additionally, node our solution is written in 250 lines of code total; a very compact solution.
\section{Lattice Generation}
Moving to multiple processors the problem of generating lattices left a number of options available. The logically simplest is to have the master generate the entire lattice and broadcast it to every other processor. This approach is obviously a huge waste of time as a large amount of data is passed to each processor. Alternatively, the master could generate segments of lattice to be passed to each processor, in this way the lattice is sent precisely once. This is better but still, this imposes a large overhead on communication when setting up the problem.\newline
Our approach is to have the master generate a single random seed which is passed to each processor. Then, each processor generates its own copy of the lattice locally using this seed. In this way, each processor gains a whole copy of the lattice with a small communication overhead of a single long-integer. \newline
We feel this solution is rather elegant although an an obvious improvement would be to generate portions of the lattice in each processor instead of the whole space. However this introduces considerable complexity into the algorithm as explicit cluster information must then be stored an exchanged. While we did not implement such a method, one is outlined later.
\section{Cluster Determination}
There has not been much change in our method for determining percolation from part I, however we make improvements to how we index our search-space so that there is less chance of processors overlapping each other. Namely we maintain the following relationship for each processor:
\begin{itemize}
\item Starting Point = myRank $*(N^{2}/P)$
\item End Point =  Start + $N^{2}/P$
\end{itemize}
Where $N$ is the edge-length of the lattice and $P$ is the number of processors in the system.\newline
This scheme guarantees that no two threads or processors will begin a search from the same lattice site. However if two searches encounter the same cluster they will both explore it fully, needlessly. This problem informed our decision to have each processor contain its own representation of the entire lattice; in this case the searches can continue without disruption. An alternative approach could include requesting a lattice segment from another process when required or maintaining a hard boundary where cluster information will be stitched together later. What follows is a basic pseudo-code representation of this scheme.
\setcounter{algorithm}{-1}
\begin{algorithm}
\caption{Na\"ive algorithm}\label{Hybrid DFS}
\begin{algorithmic}[1]
\caption{Parallel Run}
\Function {main}{argc, argv}
\State Parse\_Arguments()
\State MPI\_Init()
\State myRank $\gets$ MPI\_Comm\_rank
\State numProcs $\gets$ MPI\_Comm\_size
\If{myRank $==0$}
	\State randomSeed $\gets$ time
	\For{$i = 1$ to numProcs}
		\State MPI\_Send(randomSeed)
	\EndFor
\Else
	\State randomSeed $\gets$ MPI\_Recv(0)
\EndIf
\State $*L \gets 0$
\State $*pH \gets false$
\State $*pV \gets false$
\State parallelRun()
\If{myRank$\neq 0$}
	\State MPI\_Send($L,pH,pV$)
	\State MPI\_Finalize()
\Else
	\For{$i = 1$ to numProcs}
		\State MPI\_Recv($L,pH,pV$)
		\If{$L$>largest}
			\State largest $\gets L$ 
		\EndIf
	\EndFor
	\State OutputResults()
	\State MPI\_Finalize()
\EndIf
\EndFunction
\end{algorithmic}
\end{algorithm}
\begin{algorithm}
\caption{ThreadedSearch}\label{Parallel DFS}
\begin{algorithmic}[1]
\Function {ParallelRun}{$Sz$,$S$,$p$,$*L$,$*pH$,$*pV$,myRank, numProcs, randomSeed}
\State $Mat \gets Sz*Sz$
\State $Verts \gets Sz*Sz$
\State $Mat.populate()$
\State $\#parallel\{$
\State $seen \gets Sz*Sz$
\State $stack$
\For {$i = 0$ to $Sz*Sz$}
	\State $seen[i] \gets false$
\EndFor
\State startPoint $\gets$ myRank$Sz^{2}\div$numProcs
\State endPoint $\gets$ startPoint + $Sz^{2}\div$numProcs  
\For {$i = $startPoint to endPoint in parallel}
	\State $currSize \gets 0$
	\State $vertPerc \gets true$
	\State $horizPerc \gets true$
	\State $rows(Sz), cols(Sz)$
	\If {$Mat[i].size==0 || seen[i]==true$}
	\State continue
	\EndIf
	\State $stack.emplace\_back(i)$
	\While{$stack.notEmpty$}
		\State $curr = stack.back$
		\State $stack.pop$
	\If {$!seen[curr]$}
		\State $currSize++$
		\State $seen[i] \gets true$
		\For{$j = 0$ to $Mat[curr].size$}
			\If {$!seen[Mat[cuurr][j]]$}
				\State $stack.emplace\_back(Mat[curr][j])$
			\EndIf
		\EndFor
	\EndIf
	\State $rows[curr/Sz] \gets true$
	\State $cols[curr\%Sz] \gets true$
	\EndWhile
	\If {$currSize > *L$}
		\State $*L \gets currSize$
	\EndIf	
	\If {$checkVertPerc()$}
		\State $*pV = true$
	\EndIf
	\If {$checkHorizPerc()$}
		\State $*pH = true$
	\EndIf
\EndFor
\EndFunction
\end{algorithmic}
\end{algorithm}
\newline
\section{Expected Performance}
We expect lattice population to run on $O(E)$ time for both site and bond population methods plus the additional overhead of sending the random seed to all nodes. Searching runs in $O(V+E)$ in addition to constant factors and percolation checks. The determination of final values will incur a further penalty from passing four integers to the master-node in addition to processing each node's results.\newline
We expect an overall time complexity will be $O(E)+O(V+E+P)+O(2P)$. The first term for lattice population, second for the search and the third for communication between nodes. This condenses to $O(2E+V+3P)$.\newline
In general, we expect this scheme to provide speed-up in lower probabilities of sparse lattices in which there is a lower possibility for clusters to overlaps between nodes' and threads' search-spaces in which case we see speed-up over a sequential solution. However, in more densely populated lattices, we will not see speed-up as performance is precisely sequential, here the over-head of parallelism will become apparent through cache-misses and communication overheads.
\chapter{Parallel Hoshen-Kopelman Algorithm}
The Hoshen-Kopelman algorithm is a well known algorithm used to label clusters in percolation. This is achieved by traversing the graph labelling each node's origin. Conflicts are handled by re-labelling sites to effectively join sub-clusters together \cite{martin}. While this scheme works well in a serial implementation, allowing the problem to be parallelised is non-trivial if one does not want to replicate information or visit sites unnecessarily. The methods outlined in \cite{martin} demonstrate various methods to extend the Hoshen-Kopelman algorithm to a shared-memory architecture while those proposed in \cite{teuler} accomplish this feat in a distributed memory architecture. We propose an algorithm which combines the use of shared and distributed architectures. While we had run out of time to implement this approach, we hope it serves to show our thinking.\newline
Roughly speaking, we perform a variation of the Hoshen-Kopelman algorithm by splitting into the lattice into strips on a per-process basis, and then further into smaller strips on a per-thread basis. Each process labels clusters locally, then messages are passed to determine a global labelling scheme. Finally, a single pass over each cluster is performed to extract properties of the lattice. 
\section{Lattice Generation}
The method outlined by \cite{teuler} splits the lattice into vertical strips and maintains common boundaries between them. To achieve this, we would use a similar strategy used by our na\"ive solution. Namely, we would send a random seed to all nodes and have each generate its own strip of the lattice.\newline
However we deviate from the algorithm described as we do not maintain the common boundaries between strips in favour of our edge-list representation. By choosing to have each process consider edges leading to the right of their strip we can reconstruct the same information without the need for repeated columns. In this way, each process would generate a unique strip of the lattice to operate upon.\newline
The rule we propose for determining the index bounds of each strip is as follows:
\paragraph{The Start Point} is determined as $(S\%T)==0?(S/T)t:t<S\%T?(S\%T)+t:(S\%T)+T$
\paragraph{The End Point} is determined as StartPoint$ + (S\%T)==0?(S/N)(t+1):(t+1)<S\%T?(S\%T)+(t+1):(S\%T)+T$
\newline
Where:
\begin{itemize}
\item $T$ = total thread-numbers
\item $t$ = current thread-number
\item $S$ = size of the lattice
\end{itemize}
In this way, we can guarantee that uneven lattice sizes are split is equally as possible between the early processors. 
\section{Cluster Labelling}
One interesting property about this problem is that it behaves fractally, it can be split into smaller or combined into larger components indefinitely. Thus, when we split the whole lattice into strips, we can further split each strip on a per-thread basis and perform the same process as outlined in \cite{martin}. Each thread will search through some sub-set of the strip defined with similar method for bounds generation as above, labelling cluster origins from its own point of view. After this process concludes, a global-pass is carried out, re-labelling clusters tie-breaking conflicts with the lowest (top-left-most) label available.\newline
At this point, each strip contains a locally consistent labelling scheme which has not factored connectivity between strips. 
\section{Relaxation}
After each thread determines its local cluster-set processors begin to pass messages between each other according to the following rules
\begin{itemize}
\item Each process prepares a packet containing the origins of the connected sites on the right-side of its strip
\item Each process then sends this packet to its right neighbour (the right-most sending to the first process)
\item Each process then processes this packet, preparing a response with update origin information.
\item These packets are then sent back to their original sender to be processed
\end{itemize}
This sequence occurs $\lfloor log(P) \rfloor$ times with the each process doubling (with wrap-around) the rank of its recipient. (Sending to its 1st, 2nd, 4th, 8th neighbour, etcetera) This method also maintains a rather nice property whereby conflicts indicate a transitive cluster over three strips and thus can be labelled as the lowest value available.\newline
At the end of this sequence all strips  perform a final re-labelling and will now contain a globally consistent view of clusters.\newline
It is key to note that our description differs from that outlined in \cite{teuler} in three factors. We pass messages with wrap-around thus allowing information to propagate faster hence $\lfloor log(P) \rfloor$ rounds of messaging as opposed to the $log(P)$ rounds described in \cite{teuler} and by only considering edge-lists we eliminate repeated information in the form of shared border-columns.
\section{Lattice Property Determination}
Finally, one can determine lattice properties in a similar manner to global relabelling by sending size and percolation packets in a similar manner to those described when labelling clusters.
\section{Expected Performance}
Overall we expect a time complexity of $O(3S^{2} + \lfloor log(P) \rfloor$ where $S$ is the edge size of the lattice and $P$ is the number of processors available.
Finally, we present a high-level pseudo-code outlining this cluster determination scheme.
\begin{algorithm}
\caption{ThreadedSearch}\label{HSParallel}
\begin{algorithmic}[1]
\Function {main}{argc, argv}
	\State getOpts(bool site, int size, float prob)	
	\State MPI\_Init
	\State MPI\_Comm\_rank
	\State MPI\_Comm\_Size
	\State vector$<$int$>$ edgeList
	\State \textbf{StartPoint} $\gets (S\%T)==0?(S/T)t:t<S\%T?(S\%T)+t:(S\%T)+T$
	\State \textbf{EndPoint} $\gets$StartPoint$ + (S\%T)==0?(S/N)(t+1):(t+1)<S\%T?(S\%T)+(t+1):(S\%T)+T$ 		
	\If{site}
		\State populate(site)
	\Else
		\State populate(bond)
	\EndIf
	\State label\_locally()
	\For{$i = 1$ to $log(P)$; $i*2$}
		\State generateMessage()
		\State MPI\_Isend(message)
		\State MPI\_Irecv(message)
		\State parseMessage()
	\EndFor
	\State re-label()
	\For{$i = 1$ to $log(P)$; $i*2$}
		\State generatePropertyMessage()
		\State MPI\_Isend(message)
		\State MPI\_Irecv(message)
		\State parsePropertyMessage()
	\EndFor	
	\If{myRank$\neq0$}
		\State MPI\_Isend(results)
	\Else
		\State MPI\_Irecv(results)
		\State printResults()
	\EndIf
	\State MPI\_Finalize
\EndFunction
\end{algorithmic}
\end{algorithm}
It is a shame we did not find time to implement this algorithm. The first parts of an implementation can be found in the \textit{HKDistributed.cpp} file. It was our intention to make a comparison between this and a na\"ive solution. We hope to find time at some point to try it.
\part{Testing}
\chapter{General Approach}
Our mentality towards testing was to create as much automation as possible in order to test as many levels of scale as time permits while maintaining accurate results.\newline
In all situations results are reported 10-ary tuples of the following format:\newline
\begin{center}
$\{$Probability [0.0,1.0], Size (int), Time (12.10 float), Largest Cluster (int), Horizontal Percolation (bool), Vertical Percolation (bool), Total Percolation (bool)$\}$
\end{center}
We create batch files for each size of the lattice and each node specification which are then queued to be run. The results are then ported into an Excel sheet to be processed.\newline
We have structured our tests to try and determine which configuration of nodes leads to the most speed-up over a sequential solution.\newline
The specifications reflect that of the PBS command, the first number indicates the number of nodes, the second the number of processsors on that node.
\section{Testing Schedule}
In Table \ref{table:schedule} we define the tests ran on our solution.
\begin{table}
\caption{Testing Schedule}
\label{table:schedule}
\centering
$\begin{array}{*{7}{c}}
Nodes:Procs/Node & Size & Probability & Processors \\
$1:1$ & $1024 - 8192$ & $0.25 - 0.75$ & $1$ \\
$1:12$ & $1024 - 8192$ & $0.25 - 0.75$ & $12$ \\
$6:4$ & $1024 - 8192$ & $0.25 - 0.75$ & $24$ \\
$16:2$ & $1024 - 8192$ & $0.25 - 0.75$ & $32$ \\
$16:4$ & $1024 - 8192$ & $0.25 - 0.75$ & $64$ \\
$16:8$ & $1024 - 8192$ & $0.25 - 0.75$ & $128$ \\
\end{array}$
\end{table}
We feel this specification will give a good indication of what type of node configuration leads to speed-up. Of course the serial case is considered as a bench-mark and preliminary testing indicates that openMP defaults to six-threads per processor.\newline
As far as expected performance, given the design of our algorithm we would expect 'difficult' sequential cases to result in speed-up (low probability) with higher probabilities resulting with the sequential algorithm beating the parallelised solutions.
\chapter{Performance}
\section{Site Population}
\subsection{512}
\includegraphics[scale=2]{512Site.png}
Clearly, for a small lattice the variation between configurations will be difficult to see. Nevertheless we see that the single-node, 12 processor configuration is the slowest of them all and as we expect, the serial solution beats all parallelised specifications in higher percolation probabilities.
\subsection{1024}
\includegraphics[scale=2]{1024Site.png}
Here, we start to see the trend we expected; lower probabilities result in speed-up over the sequential solution with the sequential overtaking all others in higher probabilities. However we now begin to see a trend between the different configurations, $1:12$ is significantly slower than all others in all cases. Additionally, $16:8$ degrades rapidly in performance as the probability increases.
\subsection{2048}
\includegraphics[scale=2]{2048Site.png}
A similar story is present here, however the benefits of using parallelised solutions become more pronounced. Moreover the $16:2$ configuration is the fastest of them all. 
\subsection{4096}
\includegraphics[scale=2]{4096Site.png}
Continuing the trend, the largest lattice size we could test thoroughly shows most clearly the differences between the various cluster configurations. The general trend appears to favour a large number of nodes with new processors enabled on each. 
\subsection{8192}
Unfortunately, our trials for lattices of 8196 did not run within the allotted time limit and thus limited results are available.\newline
In fact, all but two trials exceeded the time-limit on the cluster. 
\section{Bond Population}
\subsection{512}
\includegraphics[scale=2]{512Bond.png}
A similar trend is observed between cluster configurations for a bond-population method.
\subsection{1024}
\includegraphics[scale=2]{1024Bond.png}
Again, the trend is similar to that of site-population however there is more variance here between the different configurations. In general, wall-times tend to be larger for bond-percolations. This makes sense as the percolation threshold is significantly lower for bond-population. 
\subsection{2048}
\includegraphics[scale=2]{2048Bond.png}
As with the site-population method, the fastest configuration is $16:2$ with the slowest being $1:12$. However the sequential solution seems to have greater difficulty in lower probabilities than with site-population.
\subsection{4096}
\includegraphics[scale=2]{4096Bond.png}
This was the most erratic set of results obtained especially in the sequential and $1:12$ case. This shows most strongly perhaps the benefits of parallelism as we are able to determine an outcome at least one order of magnitude faster than the sequential case.
\subsection{8192}
Unfortunately, our trials for lattices of 8196 did not run within the allotted time limit and thus limited results are available.
\chapter{Comparison}
\section{Site Population}
While it was sometimes difficult to notice any trends between cluster configurations the following trends were observed.
\begin{itemize}
\item The sequential algorithm performs well in densely populated lattices but poorly in sparsely-populated lattices
\item Conversely, parallel solutions out-performed the sequential algorithm in sparse lattices whilst under-performing in densely populated graphs.
\item More than this we noticed the $1:12$ configuration consistently performed worse than all other configurations. This indicates that highly loading one single node degrades performance
\item Conversely, $16:2$ was the best performing configuration which is just the opposite; a lot of nodes slightly loaded
\item Furthermore, we notice that performance begins to degrade slightly as more processes are introduced as the $16:8$ configuration performs the worst of the parallel solutions. This makes sense as a lattice split into smaller strips will contain more clusters which cross process search-spaces effectively highlighting the overhead involved with little benefit to performance.
\end{itemize} 
\section{Bond Population}
Bond population was consistently slower to compute than the site-population method in lower probabilities. We expect this is the result of a lower threshold percolation probability, namely the lattice is more densely populated than the site method at the same probability thus reducing the benefits of parallelism.\newline
Aside from this however, the general trends between configurations hold.
\newline
\paragraph{In summary}we see that the early benefits of shared memory parallelism employed by openMP gives performance increases over a sequential solution. However distributed memory adds to this benefit when split over a large number of nodes, each with a smaller computational load. Furthermore, we would expect this discrepancy between sequential and parallel solutions to grow as lattice size increases, however the limitations of our na\"ive solution limited the size of lattices we could ultimately test.  
\section{Short-Comings}
While our results do show some trends there is obvious room for improvement. More specifically, it would have been beneficial to test more cluster configurations of larger sizes. However to achieve this a larger amount of testing would need to be completed, and when this runs into the order of hours, it simply became infeasible. Furthermore, testing larger cluster sizes (such as 8196) would have shown a greater discrepancy between cluster configurations however in almost every case the test exceeded the allotted job time thus making testing infeasible. Additionally, there was occasional fluctuation in performance of the cluster, it would have been good to run more trials to obtain more accurate results. While we have done our best the pursuit of accuracy is one which never ends.\newline
Overall we are impressed with the scalability and configurable nature of the cluster and we hope to be given more chances to experiment with distributed code. 
\part{Instructions for Execution}
\section{Command Line Arguments}
Our program has the following command-line arguments:
\begin{itemize}
\item -s (int) size: sets the maximum size tested in the case of a full or intelligent test. Sets the size tested in a single run otherwise. Defaults to the number of processors in the system and will truncate non-even lattices.
\item -b bond: sets bool site to false if present, indicates that bond-probability should be used.
\item -p (float) prob: Sets the percolation probability to be used in a single run, defaults to 0.5 if unset or incorrectly provided. Note, it does not perform an upper bounds check, any number above 1.0 will perform like 1.0.  
\item -t (bool) threaded: If true the code will be threaded to the value openMP decides, if false, the number of threads will be set to 0.
\item -v verbose: A boolean which if enabled will write debug statements to standard out.
\end{itemize}
\section{Example Usage}
\begin{itemize}
\item ./CITS3402 -s 2048 -t -v
\item ./CITS3402 -s 2048 -t
\item ./CITS3402 -t 12
\item ./CITS3402  -s 1024 -b -p 0.4
\end{itemize}
\begin{thebibliography}{1}

\bibitem{martin} Mart\'in-Herrero, J. Alternative techniques for cluster labelling on percolation theory,  {\em Journal of Physics A: Mathematical and General}, vol. 33, 2000, pp. 1827-1840.

\bibitem{teuler} Teuler, J.M. and Gimel, J.C.  A direct parallel implementation of the Hoshen-Kopelman algorithm for distributed memory architectures,  {\em Computer Physics Communications}, vol. 130, 2000, pp. 118-129.


\end{thebibliography}
\end{document}