CPPFLAGS= -std=c++11

main.o: main.cpp
	mpic++ $(CPPFLAGS) -o CITS3402 main.cpp -fopenmp

clean:
	rm -f CITS3402
