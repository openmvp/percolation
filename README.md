# README #

Welcome to our project part II, we hope you enjoy your stay.

### How do I get set up? ###

Simply run the makefile in the directory and 'CITS3402' should be generated.
Our solution has the following command-line arguments.

* -s (int) size: sets the maximum size tested in the case of a full or intelligent test. Sets the size tested in a single run otherwise. Defaults to the number of processors in the system and will truncate non-even lattices.
* -b bond: sets bool site to false if present, indicates that bond-probability should be used.
* -p (float) prob: Sets the percolation probability to be used in a single run, defaults to 0.5 if unset or incorrectly provided. Note, it does not perform an upper bounds check, any number above 1.0 will perform like 1.0.
* -t (bool) threaded: If true the code will be threaded to the value openMP decides, if false, the number of threads will be set to 0.
* -v verbose: A boolean which if enabled will write debug statements to standard out.

For example:
* ./CITS3402 -s 2048 -t -v
* ./CITS3402 -s 2048 -t
* ./CITS3402 -t 12
* ./CITS3402  -s 1024 -b -p 0.4


Authors: Nicholas Pritchard and Callum Sullivan