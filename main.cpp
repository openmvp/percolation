//
// @author Nicholas Pritchard 21726929
// @author Callum Sullivan 21931205
//
#include <random>
#include <mpi.h>
#include "proj1.h"
#define DEFSIZE 16
void parallelRun(int size, bool site, float prob, int* largest, int* percolatesHorizontally, int* percolatesVertically, int myRank, int numProcs, time_t seed);
void printUsage();
bool verbose = false;

int main(int argc, char** argv) {
    int size = DEFSIZE;
    int sizeMax = -1;
    bool site = true;
    float prob = 0.5;
    bool threaded = false;
    verbose = false;
    //---------------------Parse Arguments----------------------->
    int opt;
    while ((opt = getopt(argc, argv, "s:bp:tv")) != -1) {
        switch (opt) {
            case 's':
                size = atoi(optarg);
                break;
            case 'b':
                site = false;
                break;
            case 'p':
                prob = atof(optarg);
                break;
            case 't':
                threaded = true;
                break;
            case 'v':
                verbose = true;
                break;
            default:
                printUsage();
                exit(EXIT_FAILURE);
        }
    }
    if (size == -1) {
        size = MAXSIZE;
    }
    if (!threaded) {
        omp_set_num_threads(1);
    }

    if (prob == -1 || prob < 0.0) {
        prob = 0.5;
    }	
    struct timeval startTime, endTime;
    //---------------------Initial Population and setup---------->
    gettimeofday(&startTime,nullptr);
    int required = MPI_THREAD_FUNNELED;
    int provided;
    MPI_Init(&argc, &argv);
    int myRank;
    int numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    if (size < numProcs) {
        printf("I'm sorry Dave, I cannot do that\n");
        size = numProcs;
    } else if (size % numProcs != 0) {
        size = size / numProcs * numProcs; //Truncate un-even lattices
    }
    time_t seed;
    if(myRank==0){
        printf("Test %i, %s, %s\n",numProcs,threaded?"True":"False",site?"Site":"Bond");
        time(&seed);
        for (int i = 1; i < numProcs; ++i) {
            MPI_Send(&seed,1,MPI_LONG_INT,i,0,MPI_COMM_WORLD);
        }
    } else {
        MPI_Status stat;
        MPI_Recv(&seed,2,MPI_LONG_INT,0,0,MPI_COMM_WORLD,&stat);
    }
    //---------------------Determination------------------------->
    int largest;
    int percolatesHorizontally = false;
    int percolatesVertically = false;
    int results[4];
    parallelRun(size,site,prob,&results[0],&results[1],&results[2],myRank,numProcs,seed);
    if(myRank!=0){
        //Send
        MPI_Send(results,3,MPI_INT,0,1,MPI_COMM_WORLD);
        MPI_Finalize();
    } else {
        largest = results[0];
        percolatesHorizontally = results[1];
        percolatesVertically = results[2];
        MPI_Status stat;
        for (int i = 1; i < numProcs; ++i) {
            MPI_Status stat;
            MPI_Recv(results,4,MPI_INT,i,1,MPI_COMM_WORLD,&stat);
            if(results[0]>largest){
                largest = results[0];
            }
            percolatesHorizontally = percolatesHorizontally || results[1];
            percolatesVertically = percolatesVertically || results[2];
        }
        gettimeofday(&endTime,nullptr);
        double duration = ((endTime.tv_sec  - startTime.tv_sec) * 1000000u + endTime.tv_usec - startTime.tv_usec) / 1.e6;
        printf("%f, %d, %12.10f, %d, %s, %s, %s\n",prob,size,duration,largest,percolatesHorizontally ? "True" : "False", percolatesVertically ? "True" : "False", percolatesHorizontally && percolatesVertically ? "True" : "False");
        MPI_Finalize();
    }
}

void parallelRun(int size, bool site, float prob, int* largest, int* percolatesHorizontally, int* percolatesVertically, int myRank, int numProcs, time_t seed) {
    //Define cross thread variables
    vector<vector<int>> matrix = vector<vector<int>>(size*size);
    vector<bool> vertices(size*size);
    //generate edge matrix
    srand(seed);
    if(site){
        float temp;
        for (int i = 0; i < size * size; ++i) {
            matrix[i] = vector<int>();
        }
        for (int i = 0; i < size * size; ++i) {
            temp = rand()/(float)RAND_MAX;
            if (temp < prob) {
                vertices[i] = true;
            }
        }
        for (int i = 0; i < size * size; ++i) {
            if (vertices[i]) {
                if (vertices[((i / size) * size) + ((i + 1) % size)]) {
                    matrix[i].emplace_back(((i / size) * size) + ((i + 1) % size));
                    if (verbose) {
                        printf("%i, %i\n", i, ((i / size) * size));
                    }
                    matrix[((i / size) * size) + ((i + 1) % size)].emplace_back(i);
                }
                if (vertices[((((i / size) + 1) % size) * size) + (i % size)]) {
                    matrix[i].emplace_back(((((i / size) + 1) % size) * size) + (i % size));
                    if (verbose) {
                        printf("%i, %i\n", i, ((((i / size) + 1) % size) * size) + (i % size));
                    }
                    matrix[((((i / size) + 1) % size) * size) + (i % size)].emplace_back(i);
                }
            }
        }
    } else{
        int currThreadNum = omp_get_thread_num();
        float temp;
        for (int i = 0; i < size * size; ++i) {
            matrix[i] = vector<int>();
        }
        for (int i = 0; i < size * size; ++i) {
            temp = rand()/(float)RAND_MAX;
            int next;
            if(temp<prob){
                next = ((i/size)*size)+((i+1)%size);
                matrix[i].emplace_back(next);
                if(verbose){
                    printf("%i, %i: thread %i\n",i,next,currThreadNum);
                }
                matrix[next].emplace_back(i);
            }
            temp = rand()/(float)RAND_MAX;
            if(temp<prob){
                next = ((((i/size)+1)%size)*size)+(i%size);
                matrix[i].emplace_back(next);
                if(verbose){
                    printf("%i, %i: thread %i\n",i,next,currThreadNum);
                }
                matrix[next].emplace_back(i);
            }
        }
    }
    //Now search the graph
    *largest = 0;
    *percolatesVertically = false;
    *percolatesHorizontally = false;
#pragma omp parallel
    {
        vector<bool> seen(size*size);
        vector<int> stack; 
        for (int i = 0; i < size * size; ++i) {
            seen[i] = false;
        }
        int start = myRank*(size*size/numProcs);
        int end = start + size*size/numProcs;	
#pragma omp for
        for (int i = start; i < end; ++i) {
            int currSize = 0;
            bool verticalPerc = true;
            bool horizontalPerc = true;
            vector<bool> rowsSeen(size), columnsSeen(size);
            if(matrix[i].size()==0 || seen[i]==true){
                continue;
            }
            for(int j = 0; j< size; j++){
                rowsSeen[j] = false;
                columnsSeen[j] = false;
            }
            stack.emplace_back(i);
            while(!stack.empty()){
                int current = stack.back();
                stack.pop_back();
                if(verbose){
                    printf("Current: %i: thread %i\n",current,omp_get_thread_num());
                }
                if(!seen[current]){
                    currSize++;
                    if(verbose){
                        printf("Examining %i: thread %i\n",current,omp_get_thread_num());
                    }
                    seen[current]=true;
                    for(int j = 0;j<matrix[current].size();++j){
                        if(!seen[matrix[current][j]]){
                            stack.emplace_back(matrix[current][j]);
                            if(verbose){
                                printf("We're putting %i on the stack: thread %i\n",matrix[current][j],omp_get_thread_num());
                            }
                        }
                    }
                }
                rowsSeen[current/size] = true;
                columnsSeen[current%size] = true;
            }

            if(currSize > *largest){
                *largest = currSize;
            }


            for(int j = 0; j<size; j++){
                if(rowsSeen[j]==false){
                    verticalPerc = false;
                    break;
                }
            }
            if(verticalPerc){
                *percolatesVertically = true;
            }
            for(int j = 0; j< size; j++){
                if(columnsSeen[j]==false){
                    horizontalPerc = false;
                    break;
                }
            }
            if(horizontalPerc){
                *percolatesHorizontally = true;
            }
        }
    };
}

void printUsage(){
    fprintf(stderr,"Usage: ./CITS3402 -s (size) int -b (useBond) -p (probability) float[0.0, 1.0]\n -t (threaded)\n -v (verbose)\n");
}

