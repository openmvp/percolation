//
// Created by Nicho on 1/11/2017.
//
#include <mpi.h>
#include "proj1.h"
#define DEFSIZE 16

void printUsage(){
    fprintf(stderr,"Usage: ./CITS3402 -s (size) int -b (useBond) -p (probability) float[0.0, 1.0]\n -t (numThreads)\n -v (verbose)\n");
}

int main(int argc, char** argv){
    int size=DEFSIZE;
    int sizeMax = -1;
    bool site = true;
    float prob = 0.5;
    bool threaded = false;
    verbose = false;
    //---------------------Parse Arguments----------------------->
    int opt;
    while((opt=getopt(argc,argv,"s:bp:tv"))!= -1){
        switch(opt){
            case 's': sizeMax = atoi(optarg); break;
            case 'b': site = false; break;
            case 'p': prob = atof(optarg); break;
            case 't': threaded = true; break;
            case 'v': verbose = true; break;
            default:
                printUsage();
                exit(EXIT_FAILURE);
        }
    }
    if(sizeMax==-1) {
        sizeMax = MAXSIZE;
    }
    if(!threaded) {
        omp_set_num_threads(1);
    }
    if(prob==-1||prob<0.0){
        prob = 0.5;
    }
    //---------------------Initial Population and setup---------->
    MPI_Init(&argc,&argv);
    int myRank;
    int numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    if(size<numProcs){
        printf("I'm sorry Dave, I cannot do that\n");
        size = numProcs;
    } else if(size%numProcs!=0){
        size = size/numProcs * numProcs; //Truncate un-even lattices
    }
    if(myRank==0){
        if(omp_get_num_threads()>numProcs){
            omp_set_num_threads(numProcs);
        }
#pragma omp parallel
        {
            thread_local mt19937 generator(random_device{}());
            uniform_real_distribution<double> distribution(0, 1);
#pragma omp for
            for (int i = 0; i < numProcs; ++i) {
                int column[size];
                for (int j = 0; j < size; ++j) {
                    if(distribution(generator)<prob){
                        column[j] = 1;
                    } else {
                        column[j] = 0;
                    }
                }
                //MPI_Request send1,send2;
                MPI_Send(column,size,MPI_INT,i,0,MPI_COMM_WORLD);
                MPI_Send(column,size,MPI_INT,(i+1)%numProcs,1,MPI_COMM_WORLD);
            }
        };
    }
    //---------------------Fetching the shared columns----------->
    //MPI_Request get1, get2;
    MPI_Status stat1, stat2;
    int columnLeft[size], columnRight[size];
    MPI_Recv(&columnRight,size+1,MPI_INT,0,0,MPI_COMM_WORLD,&stat1);
    MPI_Recv(&columnLeft,size+1,MPI_INT,0,1,MPI_COMM_WORLD,&stat2);
    //MPI_Wait(&get1,&stat1);
    //MPI_Wait(&get2,&stat2);
    if(verbose){
        char msg[size*2+128];
        char num[10];
        msg[0] = '\0';
        sprintf(msg,"Hello, my name is %i and this is %i\n",myRank, size);
        for (int m = 0; m < size; ++m) {
            num[0] = '\0';
            sprintf(num,"%d",columnLeft[m]);
            strcat(msg,num);
        }
        strcat(msg,"\n");
        for (int n = 0; n < size; ++n) {
            num[0] = '\0';
            sprintf(num,"%d",columnRight[n]);
            strcat(msg,num);
        }
        strcat(msg, "\n");
        if(myRank!=0){
            MPI_Send(msg,size*2+128,MPI_CHAR,0,2,MPI_COMM_WORLD);
        } else {
            printf("%s",msg);
            char display[2*size+129];\
        MPI_Status potato;
            for (int i = 1; i < numProcs; ++i) {
                display[0] = '\0';
                MPI_Recv(display,size*2+129,MPI_CHAR,i,2,MPI_COMM_WORLD,&potato);
                printf("%s",display);
            }
        }
    }
    /*
     * We have the following relationship between processors to determine their indexes
     * # columns total = N/P + P
     * # cols/Processor = N/P + 1
     * 1st Index = p/P * N^2
     * Last Index = (1st + N(1+N/P)-1)%N
     * This does mean that our solution will only work with problems with the following properties
     * N%P == 0, N>=P i.e. the lattice size must be some multiple of the number of processors
    */

    MPI_Finalize();
}
