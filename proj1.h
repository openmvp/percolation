//
// Created by Nicholas on 11/09/2017.
//
#ifndef PROJECT_1_PROJ1_H
#define PROJECT_1_PROJ1_H
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>
#include <vector>
#include <string.h>
#include <math.h>
#include <random>

#define MAXSIZE 2048
extern bool verbose;
using namespace std;

#endif //PROJECT_1_PROJ1_H